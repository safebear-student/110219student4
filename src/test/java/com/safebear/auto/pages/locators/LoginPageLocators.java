package com.safebear.auto.pages.locators;

import org.openqa.selenium.By;

public class LoginPageLocators {

    //fields
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.xpath(".//input[@name='psw']");

    //buttons
    private By loginButtonLocator = By.id("enter");

    //messages
    private By warningMessageLocator = By.xpath(".//p[@id='rejectLogin']/b");
}
