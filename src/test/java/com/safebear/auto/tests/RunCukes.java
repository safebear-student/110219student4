package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        //This is where my html report will be in the target cucumber directory
        plugin = {"pretty", "html:target/cucumber"},
        //This is changing to 'not @to-do' in future cucumber releases
        tags = "~@to-do",
        //This is where our tests will be created
        glue = "com.safebear.auto.tests",
        //This is running all the features in this folder
        features = "classpath:toolslist.features/login.feature"
)
public class RunCukes extends AbstractTestNGCucumberTests {
}